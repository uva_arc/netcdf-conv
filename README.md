# NetCDF-conv

Converts an HDF5 data store to a valid NetCDF4 file with CF conventions. This application was prepared in partnership with the HDF Group with support from NSF (grant #2022639) for the UVA-ARC project.

## Getting started

This application depends on few Python packages:

* h5py
* numpy
* cfdm

It is always a good idea to get started by creating a virtual environment with all the dependencies installed:

```
$ python3 -m venv netcdf-conv 
$ source netcdf-conv/bin/activate
$ pip install -r requirements.txt
```

Voilà, now you are ready to use the application!

## Usage

`NetCDF-conv` is a command-line utility with the goal of helping to automate conversion across formats. You can call the application and pass an HDF5 file to it or, alternatively, an HSDS endpoint (hdf5://):

```
$ python src/netcdf-conv.py testfile.h5
```

The application will return a NetCDF4 file with `.nc` extension and will name it with the current timestamp (in UTC).

## Authors and licensing

See AUTHORS and LICENSE files for details.

